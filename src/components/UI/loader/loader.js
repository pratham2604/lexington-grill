import React from 'react';
import { Grid } from 'semantic-ui-react';

export default () => (
  <Grid textAlign='center' className="page-loader" verticalAlign='middle'>
    <Grid.Column width={6}>
      <div className="lds-spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </Grid.Column>
  </Grid>
)