import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Auth extends Component {
  state = {
  }

  componentDidMount() {
    this.redirect();
  }

  componentDidUpdate() {
    this.redirect();
  }

  redirect = () => {
    const { location, history } = this.props;
    if (location.pathname !== '/') {
      history.push('/');
    }
  }

  render() {
    const { Template, pageTitle, hideFooter, Container, Component, routeProps } = this.props;

    const layout = Container ?
      <Container props={routeProps} Layout={Component} /> :
      <Component props={routeProps}/>

    return (
      <Template pageTitle={pageTitle} hideFooter={hideFooter}>
        {layout}
      </Template>
    );
  }
}

export default (withRouter(Auth));