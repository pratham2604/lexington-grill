import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';

let fetchMenuSubscription = null;

export const fetchMenuItems = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_MENU_REQUEST});
    fetchMenuSubscription = Firestore.collection("restaurant_menu").doc(restaurantId).onSnapshot(snapshot => {
      if (snapshot.data()) {
        const menuData = snapshot.data();
        const { items, id } = menuData;
        dispatch({type: TYPES.FETCH_MENU_SUCCESS, data: items, menuId: id});
        return;
      } else {
        dispatch({type: TYPES.FETCH_MENU_SUCCESS, data: [], menuId: null});
      }
    });
  }
}

export const unsubscribeMenu = () => {
  fetchMenuSubscription();
}

export const updateMenu = (data, action) => {
  return (dispatch) => {
    dispatch({type: TYPES.ADD_MENU_REQUEST});
    Firestore.collection("restaurant_menu").doc(data.restaurantId)[action](data)
    .then((snapshot) => {
      dispatch({type: TYPES.ADD_MENU_SUCCESS});
    }).catch((error) => {
      toast.error(error);
    });
  }
}

export const fetchFoodCategories = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_MENU_CATEGORIES_REQUEST});
    Firestore.collection("food_categories").where("restaurantId", "==", restaurantId).get().then(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_MENU_CATEGORIES_SUCCESS, data});
    });
  }
}

export const updateOrder = (order) => {
  const data = Object.assign({}, order, {
    id: 'ongoing-order',
  });
  return (dispatch) => {
    dispatch({type: TYPES.CREATE_ORDER_SUCCESS, data})
  }
}

export const placeOrder = (data) => {
  return (dispatch) => {
    dispatch({type: TYPES.ADD_MENU_REQUEST});
    data.id = uuidv4();

    Firestore.collection("orders").where("restaurantId", "==", data.restaurantId).get()
    .then(snapshot => {
      const totalOrders = [];
      snapshot.docs.forEach((val) => {
        if (val && val.data()) {
            totalOrders.push(val.data());
        }
      });
      const today = moment().format("DD MM YYYY");
      const todaysOrders = totalOrders.filter(order => moment(order.recordedAt).format("DD MM YYYY") === today);
      data.number = (todaysOrders + 1);
      Firestore.collection("orders").doc(data.id).set(data)
      .then((snapshot) => {
        dispatch({type: TYPES.PLACE_ORDER_SUCCESS});
        toast.success("Order Placed successfully");
        setTimeout(() => { window.location.replace('/'); }, 3000);
        // dispatch({type: TYPES.ADD_MENU_SUCCESS});
      }).catch((error) => {
        toast.error(error);
      });
    }).catch((error) => {
      toast.error(error);
    });
  }
}

export const fetchTable = (id, showError = false) => {
  return (dispatch) => {
    Firestore.collection("restaurant_table").doc(id).get()
      .then((doc) => {
        if (doc.exists) {
          const table = doc.data();
          dispatch({type: TYPES.FETCH_TABLE_SUCCESS, data: table});
        }
      }).catch((error) => {
        console.log(error);
        showError && toast.error(error);
      });
  }
}