import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';
import { v4 as uuidv4 } from 'uuid';
import { SEND_SMS } from '../lib/smsHelper';

export const fetchRestaurant = (restaurantId) => {
  return (dispatch) => {
    Firestore.collection("restaurants").doc(restaurantId).get().then(restaurantDoc => {
      if (restaurantDoc.exists) {
        const restaurant = restaurantDoc.data();
        dispatch({type: TYPES.FETCH_RESTAURANT_SUCCESS, data: restaurant});
        return;
      }

      const message = 'Restaurant not found';
      toast.error(message);
    }).catch(err => {
      toast.error(err);
    });
    return;
  }
}

let fetchOrdersSubscription = null;

export const fetchOrders = (restaurantId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_ORDERS_REQUEST});
    fetchOrdersSubscription = Firestore.collection("orders").where("restaurantId", "==", restaurantId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      dispatch({type: TYPES.FETCH_ORDERS_SUCCESS, data});
    });
  }
}

let fetchUseOrdersSubscription = null;

export const fetchOrdersForUser = (customerId) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_ORDERS_REQUEST});
    fetchUseOrdersSubscription = Firestore.collection("orders").where("customerId", "==", customerId).onSnapshot(snapshot => {
      const data = snapshot.docs.map((val) => {
        return val.data();
      });
      const uniqRestaurantIds = [];
      data.forEach(order => {
        const isRestaurantPresent = uniqRestaurantIds.find(id => id === order.restaurantId);
        if (!isRestaurantPresent) {
          uniqRestaurantIds.push(order.restaurantId);
        }
      });
      return Promise.all(uniqRestaurantIds.map(id => {
        return Firestore.collection("restaurants").doc(id).get().then(restaurantDoc => {
          if (restaurantDoc.exists) {
            return restaurantDoc.data();
          }
    
          return false;
        })
      })).then(results => {
        const updatedData = data.map(order => {
          const restaurant = results.find(restaurant => restaurant.id === order.restaurantId) || {};
          return Object.assign({}, order, {
            restaurantName: restaurant.name || '',
          });
        });
        dispatch({type: TYPES.FETCH_ORDERS_SUCCESS, data: updatedData});
      })
    });
  }
}

export const unsubscribeUserOrders = () => {
  fetchUseOrdersSubscription && fetchUseOrdersSubscription();
}


export const unsubscribeOrders = () => {
  fetchOrdersSubscription();
}

export const addGuestUser = (data) => {
  return (dispatch) => {
    dispatch({type: TYPES.ADD_GUEST_USER_REQUEST});
    data.id = uuidv4();
    Firestore.collection("users").doc(data.id).set(data)
    .then((doc) => {
      Firestore.collection("users").doc(data.id).get().then(doc => {
        if (doc.exists) {
          const data = doc.data();
          dispatch({type: TYPES.ADD_GUEST_USER_SUCCESS, data});
        }
      });
    }).catch((error) => {
      console.log(error);
    });
  }
}

export const fetchGuestUser = (userId) => {
  return (dispatch) => {
    Firestore.collection("users").doc(userId).get().then(doc => {
      if (doc.exists) {
        const data = doc.data();
        dispatch({type: TYPES.ADD_GUEST_USER_SUCCESS, data});
      }
    }).catch(err => {
      console.log(err);
    });
  }
}

export const updateOrderStatus = (order, smsData) => {
  const { send, receiver, message } = smsData || {};
  return (dispatch) => {
    // dispatch({type: TYPES.UPDATE_ORDER_STATUS_REQUEST});
    Firestore.collection("orders").doc(order.id).update(order).then(res => {
      // dispatch({type: TYPES.UPDATE_ORDER_STATUS_SUCCESS});
      if (send) {
        const data = Object.assign({}, { receiver, message });
        SEND_SMS(data).then(res => {
          console.log(res);
        }).catch(err => {
          console.log(err);
        })
      }
    }).catch(err => {
      toast.error(err);
    })
  }
}

export const getUserDetails = (order, onSuccess) => {
  const { customerId } = order;
  Firestore.collection("users").doc(customerId).get().then(doc => {
    if (doc.exists) {
      const user = doc.data();
      onSuccess(order, user);
      return;
    }

    toast.error('User not found');
  }).catch(err => {
    toast.error(err);
  })
}

export const verifyCode = (id, onSuccess, onFailure) => {
  return (dispatch) => {
    Firestore.collection("restaurant_table").doc(id).get()
      .then((doc) => {
        if (doc.exists) {
          const table = doc.data();
          onSuccess(table);
          return;
        }
        
        onFailure('Invalid code entered')
      }).catch((error) => {
        onFailure(error);
      });
  }
}
