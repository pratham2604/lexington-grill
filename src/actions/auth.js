import { Firestore, Firebase } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';
import { v4 as uuidv4 } from 'uuid';

export const fetchAuth = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_AUTH_REQUEST});
    return Firebase.auth().onAuthStateChanged(user => {
      if (user) {
        const { email, emailVerified, uid, refreshToken, displayName, photoURL } = user;
        const data = Object.assign({}, { email, emailVerified, uid, displayName, photoURL });
        dispatch({
          type: TYPES.FETCH_AUTH_SUCCESS,
          data,
          token: refreshToken,
        });
      } else {
        const message = 'User auth not present';
        dispatch({type: TYPES.FETCH_AUTH_FAILURE, message});
      }
    });
  }
}

export const signUp = (data) => {
  const { email, password } = data;
  return (dispatch) => {
    dispatch({type: TYPES.SIGNUP_REQUEST});
    Firebase.auth().createUserWithEmailAndPassword(email, password).then(response => {
      const { user } = response;
      if (user) {
        const { email, uid } = user;
        const restaurantId = uuidv4();
        const restaurantUser = Object.assign({}, { email, restaurantId });
        const { name, phoneNumber, description, type, thumb, photos, location, fileName, gstNo, currency } = data;
        const restaurantData = Object.assign({}, {
          name, phoneNumber, description, type, thumb, photos, location, fileName, gstNo, currency
        }, {
          id: restaurantId
        });
        if (!gstNo) {
          delete restaurantData.gstNo;
        }

        Promise.all([
          Firestore.collection("restaurant_users").doc(uid).set(restaurantUser),
          Firestore.collection("restaurants").doc(restaurantId).set(restaurantData)
        ]).then(res => {
          console.log(res);
        }).catch(error => {
          console.log(error);
          toast.error(error);
        });
      } else {
        const message = 'Unable to register account';
        toast.error(message);
        dispatch({type: TYPES.SIGNUP_FAILURE});
      }
    }).catch(err => {
      const { message = 'Unable to register account' } = err;
      toast.error(message);
      dispatch({type: TYPES.SIGNUP_FAILURE});
    });
  }
}

export const login = (data) => {
  const { email, password } = data;
  return (dispatch) => {
    dispatch({type: TYPES.LOGIN_REQUEST});
    Firebase.auth().signInWithEmailAndPassword(email, password).catch(err => {
      const { message = 'Unable to login' } = err;
      toast.error(message);
      dispatch({type: TYPES.LOGIN_FAILURE});
    });
  }
}

export const signOut = () => {
  return (dispatch) => {
    dispatch({type: TYPES.SIGNOUT_REQUEST});
    return Firebase.auth().signOut().then(() => {
      dispatch({type: TYPES.CLEAR_RESTAURANT_DATA});
    });
  }
}