import React, { Component } from 'react';
import { Grid, Header, Container, Image } from 'semantic-ui-react';
import Logo from '../../../assets/SmartOrdr-Logo-White.svg';
import Landing from './landing';
import Categories from './categories';

export default class extends Component {
  render() {
    const { verifyCode } = this.props;

    return (
      <div>
        <Landing />
        <Categories verifyCode={verifyCode} />
        <Footer />
      </div>
    )
  }
}

class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
        <Container>
          <Grid stackable>
            <Grid.Row>
              <Grid.Column width={4}>
                <div className="logo-container">
                  <Image src={Logo} size="mini" className="logo"/>
                  <Header as="h1" className="app-title">Smart Ordr</Header>
                </div>
              </Grid.Column>
              <Grid.Column width={8}></Grid.Column>
              <Grid.Column width={4} className="copy-right">
                &copy; 2020 Smart Ordr
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
    )
  }
}