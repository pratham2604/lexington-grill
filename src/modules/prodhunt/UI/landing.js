import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';

export default class Landing extends Component {
  render() {
    return (
      <div className="prodhunt-landing-container">
        <Container>
          <h1 className="title">
            Smart Ordr
          </h1>
          <div className="description">
            Making your ordering digital
          </div>
        </Container>
      </div>
    )
  }
}
