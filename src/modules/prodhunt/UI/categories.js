import React, { Component } from 'react';
import { Grid, Container, Segment, Image, Input, Button } from 'semantic-ui-react';
import UserFeatureOne from '../../../assets/1.svg';
import UserFeatureTwo from '../../../assets/2.svg';
import UserFeatureThree from '../../../assets/3.svg';
import BusinessFeatureOne from '../../../assets/4.svg';
import BusinessFeatureTwo from '../../../assets/5.svg';
import BusinessFeatureThree from '../../../assets/6.svg';
import QRCode from '../../../assets/0F9B97.png';
import * as Analytics from '../../../lib/analytics';
const adminLink = 'https://admin.smartordr.com/';

export default class Categories extends Component {
  state = {
    code: '',
  }

  componentDidMount() {
    Analytics.logPageView();
  }

  onChange = (e, {name, value}) => {
    this.setState({
      [name]: value,
    });
  }

  onSignUpClick = () => {
    const event = {
      category: 'Navigation',
      action: `Clicked Setup Smart Ordering`,
      label: 'Home'
    };
    Analytics.logEvent(event);
  }

  onSubmit = () => {
    const event = {
      category: 'Order',
      action: `Clicked Start Ordering now`,
      label: 'Prodhunt - Table Code'
    };
    Analytics.logEvent(event);
    const { code } = this.state;
    this.props.verifyCode(code);
  }

  render() {
    return (
      <div className="categories-container">
        <Container>
          <Grid stackable columns="equal">
            <Grid.Row>
              <Grid.Column>
                <Segment className="category-container">
                  <div className="title">
                    For User
                  </div>
                  <div className="invite-container">
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={7} className="qr-code-section">
                          <div className="invite-title">
                            Scan QR
                          </div>
                          <Image src={QRCode} size="small" className="invite-qr-code"/>
                        </Grid.Column>
                        <Grid.Column width={2} className="separtor">
                          Or
                        </Grid.Column>
                        <Grid.Column width={7} className="table-section">
                          <div className="invite-title">
                            Enter Code <span className="sub-title">(eg. 0F9B97)</span>
                          </div>
                          <div className="order-container">
                            <Input fluid placeholder="Enter order code" className="order-input" name="code" onChange={this.onChange}></Input>
                            <Button fluid color="linkedin" className="order-button" onClick={this.onSubmit}>Start Ordering Now</Button>
                          </div>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                  <div className="features-container">
                    <Grid stackable columns="equal">
                      <Grid.Row>
                        {USERS_FEATURES.map((feature, index) => <Feature data={feature} key={index}/>)}
                      </Grid.Row>
                    </Grid>
                  </div>
                </Segment>
              </Grid.Column>
              <Grid.Column>
                <Segment className="category-container">
                  <div className="title">
                    For Business
                  </div>
                  <div className="invite-container">
                    <div className="signup-container">
                      <div className="order-container">
                        <Button as="a" color="linkedin" className="order-button" href={adminLink} target="_blank" onClick={this.onSignUpClick}>Sign Up</Button>
                      </div>
                    </div>
                  </div>
                  <div className="features-container">
                    <Grid stackable columns="equal">
                      <Grid.Row>
                        {BUSINESS_FEATURES.map((feature, index) => <Feature data={feature} key={index}/>)}
                      </Grid.Row>
                    </Grid>
                  </div>
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
    )
  }
}

class Feature extends Component {
  render() {
    const { image, description } = this.props.data;
    return (
      <Grid.Column>
        <Segment className="feature-container">
          <Image src={image} size="tiny" className="feature-image"/>
          <div className="feature-description">
            {description}
          </div>
        </Segment>
      </Grid.Column>
    )
  }
}

const USERS_FEATURES = [{
  image: UserFeatureOne,
  description: 'Scan QR to access digital catalogue/menu'
}, {
  image: UserFeatureTwo,
  description: 'Order service or product'
}, {
  image: UserFeatureThree,
  description: 'Track order status. Pay Offline/Online'
}];

const BUSINESS_FEATURES = [{
  image: BusinessFeatureOne,
  description: 'Create digital catalogue or menu for local services'
}, {
  image: BusinessFeatureTwo,
  description: 'Reduce order time and overheads'
}, {
  image: BusinessFeatureThree,
  description: 'Earn incremental revenues'
}];