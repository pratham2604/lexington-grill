export const CATEGORIES = [{
  id: 'small-plates-cold',
  name: 'Small plates cold'
}, {
  id: 'steak-selection',
  name: 'Steak Selection'
}, {
  id: 'lexington-mains',
  name: 'Lexington Mains'
}, {
  id: 'soups',
  name: 'Soups'
}, {
  id: 'house-dry-aged-steak',
  name: 'House Dry Aged Steak'
}, {
  id: 'small-plates-hot',
  name: 'Small Plates Hot'
}, {
  id: 'sauces',
  name: 'Sauces'
}, {
  id: 'sides',
  name: 'Sides'
}]

export const MENU = [{
  name: 'Matured & Smoked Steak Tartare',
  ingredients: 'Confit Egg Yolk, Sesame Relish.',
  price: '95',
  category: 'small-plates-cold'
}, {
  name: 'Beet Cured Norwegian Salmon',
  ingredients: 'Truffle Ponzu, Avocado, Seaweed, Tapioca Chip',
  price: '90',
  category: 'small-plates-cold'
}, {
  name: 'Burratina & Heritage Tomatoes',
  ingredients: 'Textures of Tomatoes, Basil Oil',
  price: '85',
  category: 'small-plates-cold',
  tooltip: 'Vegetarian',
  tooltipContent: '(V)'
}, {
  name: 'The Waldorf Salad',
  ingredients: 'Bresaola, Celeriac, Candid Walnut, Granny Smith Apple',
  price: '55',
  category: 'small-plates-cold',
  tooltip: 'Vegetarian, Nuts',
  tooltipContent: '(V, N)'
},{
  name: 'Lamb Shoulder Bourguignon',
  ingredients: 'Whipped Parsnip , Crispy Beef Speck',
  price: '185',
  category: 'lexington-mains',
  tooltip: 'Alcohol',
  tooltipContent: '(A)'
},{
  name: 'Milk Fed Veal Osso Buco',
  ingredients: 'Pumpkin Polenta, Mojo Verde',
  price: '180',
  category: 'lexington-mains'
},{
  name: 'Roasted Celeriac Veloute',
  ingredients: 'Essence of Lavender, Soft Herb',
  price: '60',
  category: 'soups',
  tooltip: 'Vegetarian',
  tooltipContent: '(V)'
},{
  name: 'Baby Spatchcock',
  ingredients: 'Roquette Salad, Purple Potatoes',
  price: '175',
  category: 'lexington-mains'
},{
  name: 'Chargrilled King Prawns',
  ingredients: 'Coral Seaweed butter, Cilantro Mash',
  price: '220',
  category: 'lexington-mains'
},{
  name: 'Perigod Crusted Fois Gras',
  ingredients: 'Black & Yellow Puree, Spicy Turk Fig Pastilla',
  price: '75',
  category: 'small-plates-hot'
},{
  name: 'Pistachio Wagyu Skewers Chargrilled',
  ingredients: 'BBQ Chipotle',
  price: '105',
  category: 'small-plates-hot',
  tooltip: 'Nuts',
  tooltipContent: '(N)'
},{
  name: 'Baked Brie & Sour Dough',
  ingredients: 'Slow Cooked Shallot, Taylors Port',
  price: '65',
  category: 'small-plates-hot',
  tooltip: 'Alcohol, Vegetarian',
  tooltipContent: '(A, V)'
},{
  name: 'Charred Pulpo',
  ingredients: 'Lime Aioli, Bird’s Eye Chimichurri',
  price: '120',
  category: 'small-plates-hot'
},{
  name: 'Forest Mushroom Risotto',
  ingredients: 'Truffle, Hazelnut Beurre Noisette',
  price: '95',
  category: 'lexington-mains',
  tooltip: 'Vegetarian, Nuts',
  tooltipContent: '(V, N)'
},{
  name: 'Hermitage Jus',
  price: '12',
  category: 'sauces',
  tooltip: 'Alcohol',
  tooltipContent: '(A)'
},{
  name: 'Wild Mushroom Cream',
  price: '12',
  category: 'sauces',
},{
  name: 'Pommery Mustard',
  price: '12',
  category: 'sauces',
},{
  name: 'Lemon Butter',
  price: '12',
  category: 'sauces',
},{
  name: 'Béarnaise',
  price: '12',
  category: 'sauces',
},{
  name: 'Stilton Cheese',
  price: '12',
  category: 'sauces',
},{
  name: 'Black Peppercorn',
  price: '12',
  category: 'sauces',
},{
  name: 'Chargrilled Brocollini',
  price: '30',
  category: 'sides',
},{
  name: 'Posh Chips',
  price: '30',
  category: 'sides',
},{
  name: 'Creamed Baby Spinach',
  price: '30',
  category: 'sides',
},{
  name: 'Assiette of Wild Mushrooms',
  price: '30',
  category: 'sides',
},{
  name: 'Macaroni and Cheese',
  price: '30',
  category: 'sides',
},{
  name: 'Potato and Fennel Gratin',
  price: '30',
  category: 'sides',
},{
  name: 'Whipped Mash Potato Classic/Truffle',
  price: '30',
  category: 'sides',
},{
  name: 'Salad of Rouquette and Radicchio',
  price: '30',
  category: 'sides',
}, {
  name: 'USDA ‘Fox River’ Certified Prime American Beef',
  ingredients: [{
    name: 'Rib Eye Steak',
    weight: '300g.',
    cost: '210'
  }, {
    name: 'Tenderloin',
    weight: '220g.',
    cost: '235'
  }, {
    name: 'NY Strip Loin',
    weight: '350g.',
    cost: '205'
  }],
  category: 'steak-selection',
  type: 'special'
}, {
  name: 'Australian Wagyu Full Blood Grade 6-7',
  ingredients: [{
    name: 'Rib Eye Steak',
    weight: '300g.',
    cost: '455'
  }, {
    name: 'Tenderloin',
    weight: '220g.',
    cost: '395'
  }],
  category: 'steak-selection',
  type: 'special'
}, {
  name: 'USDA ‘Fox River’ Certified Prime Dry Aged',
  ingredients: [{
    name: 'Rib Eye Steak',
    weight: '300g.',
    cost: '295'
  }, {
    name: 'Gentleman Jack Whisky NY Strip Loin',
    weight: '250g.',
    cost: '270'
  }],
  category: 'house-dry-aged-steak',
  type: 'special'
}, {
  name: 'Grills to Share',
  ingredients: [{
    name: 'Châteaubriand',
    weight: '500g.',
    cost: '530'
  }, {
    name: 'Bone-In Rib Eye',
    weight: '800g.',
    cost: '395'
  }, {
    name: 'Land & Sea – Prime Tenderloin, Braised Lamb, King Prawns',
    cost: '645'
  }],
  category: 'house-dry-aged-steak',
  type: 'special'
}]