import React, { Component, Fragment } from 'react';
import { Header, Icon } from 'semantic-ui-react';
import Menu from './menu';
import _ from 'lodash';
import cx from 'classnames';

export default class extends Component {
  state = {
    activeIndex: 0,
  }

  render() {
    const { categories = [], menu = [], fixed } = this.props;
    const otherMenus = menu.filter(menuItem => !categories.some(category => category.id === menuItem.category));
    const otherMenuList = _.sortBy(otherMenus, 'thumbnail').reverse();

    return (
      <Fragment>
        <div className="menu-list-container">
          {categories.map((category, index) => {
            const { name, id } = category;
            const menuList = menu.filter(menuItem => menuItem.category === id );
            if (menuList.length === 0) {
              return null;
            }

            return (
              <Category key={index} category={category} menuList={menuList} />
            )
          })}
          {otherMenuList.length > 0 && <Category menuList={otherMenuList} />}
        </div>
      </Fragment>
    )
  }  
}

class Category extends Component {
  state = {
    showMenu: true,
  }

  toggleMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu,
    });
  }

  render() {
    const { showMenu } = this.state;
    const { category = {}, menuList = [] } = this.props;
    const { name, id } = category;
    const icon = showMenu ? 'chevron down' : 'chevron up';
    const titleClass = cx('category-title', {
      'hide-menu': !showMenu
    });

    return (
      <div className="category-container" id={id}>
        <div className="separator"></div>
        <div className={titleClass}>
          <div className="title-content">{name || 'Others'}</div>
          <div className="toggle-icon" onClick={this.toggleMenu}>
            <Icon name={icon} />
          </div>
        </div>
        {showMenu &&
          <div>
            {_.sortBy(menuList, 'thumbnail').reverse().map((menuItem, index) => {
              return (
                <Menu menu={menuItem} key={index} />
              )
            })}
          </div>
        }
      </div>
    )
  }
}
