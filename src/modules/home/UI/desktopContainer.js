import React, { Component } from 'react';
import { Grid, Container, Button, Header } from 'semantic-ui-react';
import Categories from './categories';
import MenuList from './menuList';
import { CATEGORIES, MENU } from './../constants/data';
import { Visibility } from 'semantic-ui-react';

export default class extends Component {
  state = {
    pixelsPassed: 0
  }

  handleUpdate = (e, { calculations}) => {
    const { percentagePassed, pixelsPassed } = calculations;
    this.setState({
      pixelsPassed,
    })
  }

  render() {
    const { pixelsPassed } = this.state;
    return (
      <div>
        <Visibility onUpdate={this.handleUpdate}>
          <div className="banner-container"></div>
          <Container>
            <Header as="h1" style={{textAlign: 'center', fontSize: '3em', marginBottom: '1em'}}>
              Lexington Grill
              <div>
                <Button circular color='facebook' icon='facebook' />
                <Button circular color='twitter' icon='twitter' />
                <Button circular color='linkedin' icon='linkedin' />
                <Button circular color='google plus' icon='google plus' />
                <Button circular color='instagram' icon='instagram' />
              </div>
            </Header>
            <Header as="h4" style={{textAlign: 'center', width: '80%', margin: 'auto', marginBottom: '2em'}}>
              <font>All prices are in United Arab Emirates Dirham and inclusive of 10% service charge and 5% VAT. If you have a food allergy or special dietary requirement please inform a member of our team.</font>
            </Header>
          </Container>
          <Container>
            <Grid>
              <Grid.Row>
                <Grid.Column width={4}>
                  <Categories categories={CATEGORIES} pixelsPassed={pixelsPassed} />
                </Grid.Column>
                <Grid.Column width={8}>
                  <MenuList categories={CATEGORIES} menu={MENU} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </Visibility>
      </div>
    )
  }
}