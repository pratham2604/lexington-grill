import React, { Component } from 'react';
import cx from 'classnames';
import Scrollchor from 'react-scrollchor';


export default class extends Component {
  state = {
    activeIndex: 0,
  }

  updateIndex = (index) => {
    this.setState({
      activeIndex: index,
    });
  }

  render() {
    const { categories, pixelsPassed = 0 } = this.props;
    const { activeIndex } = this.state;
    const style = pixelsPassed > 600 ? {position: 'fixed', top: '5em'} : {};

    return (
      <div className="categories-container" style={style}>
        {categories.map((category, index) => {
          const { id, name } = category;
          const categoryClass = cx('cateogry', {
            'active': index === activeIndex,
          });
          const link = `#${id}`;
          return (
            <Scrollchor to={link} animate={{offset: -90}} key={id}>
              <div key={index} className={categoryClass} onClick={this.updateIndex.bind(this, index)}>
                <span className="name">{name}</span>
              </div>
            </Scrollchor>
          )
        })}
      </div>
    )
  }  
}
