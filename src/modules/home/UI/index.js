import React, { Component } from 'react';
import DesktopContainer from './desktopContainer';
import MobileContainer from './mobileContainer';

export default class extends Component {
  render() {
    return (
      <div className="home-container">
        <div className="desktop-container">
          <DesktopContainer />
        </div>
        <div className="mobile-container">
          <MobileContainer />
        </div>
      </div>
    )
  }
}