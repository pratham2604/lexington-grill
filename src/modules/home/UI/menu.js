import React, { Component } from 'react';
import { Image, Popup, Grid } from 'semantic-ui-react';

export default class Menu extends Component {
  render() {
    const { menu } = this.props;
    const { name, price, thumbnail, ingredients, tooltip, tooltipContent, type } = menu || {};
    const currency = 'DIRHAM'

    if (type === 'special') {
      return (
        <div className="menu-item">
          <div className="menu-item-container">
            {thumbnail && <Image src={thumbnail} size="tiny" className="menu-image"/>}
            <div className="menu-info">
              <div className="menu-name">
                <span><font>{name}</font></span>
              </div>
              <div className="ingredients">
                {ingredients.map((ingredient, index) => {
                  return (
                    <Grid key={index}>
                      <Grid.Row>
                        <Grid.Column width={9}><font>{ingredient.name}</font></Grid.Column>
                        <Grid.Column width={3} style={{fontStyle: 'normal', textAlign: 'right'}}>{ingredient.weight}</Grid.Column>
                        <Grid.Column width={4} style={{fontStyle: 'normal', fontWeight: 'bold', textAlign: 'right'}}>{CURRENCY_MAP[currency]} {ingredient.cost}</Grid.Column>
                      </Grid.Row>
                    </Grid>
                  )
                })}
              </div>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className="menu-item">
        <div className="menu-item-container">
          {thumbnail && <Image src={thumbnail} size="tiny" className="menu-image"/>}
          <div className="menu-info">
            <div className="menu-name">
            <span><font>{name}</font></span>
              {tooltip && <Popup content={tooltip} trigger={<span style={{cursor: 'pointer', marginLeft: '1em'}}>{tooltipContent}</span>} />}
            </div>
            <div className="ingredients">
            <font>{ingredients}</font>
            </div>
          </div>
          <div className="price">
            {CURRENCY_MAP[currency]} {price}
          </div>
        </div>
      </div>
    )
  }
}

const CURRENCY_MAP = {
  INR: '₹',
  DOLLAR: '$',
  EURO: '€',
  POUND: '£',
  DIRHAM: 'د.إ'
}
