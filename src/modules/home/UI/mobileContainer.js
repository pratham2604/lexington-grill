import React, { Component, Fragment } from 'react';
import Menu from './menu';
import { Button, Icon, Container, Header } from 'semantic-ui-react';
import cx from 'classnames';
import _ from 'lodash';
import { CATEGORIES, MENU} from '../constants/data';

export default (props) => {

  return (
    <Fragment>
      <div className="banner-container mobile-banner"></div>
      <Container>
        <Header as="h1" style={{textAlign: 'center', fontSize: '3em', marginBottom: '1em'}}>
          Lexington Grill
          <div>
            <Button circular color='facebook' icon='facebook' />
            <Button circular color='twitter' icon='twitter' />
            <Button circular color='linkedin' icon='linkedin' />
            <Button circular color='google plus' icon='google plus' />
            <Button circular color='instagram' icon='instagram' />
          </div>
        </Header>
        <Header as="h4" style={{textAlign: 'center', width: '80%', margin: 'auto', marginBottom: '2em'}}>
          All prices are in United Arab Emirates Dirham and inclusive of 10% service charge and 5% VAT. If you have a food allergy or special dietary requirement please inform a member of our team.
        </Header>
      </Container>
      <div className="menu-list-container" style={{padding: '2em'}}>
        <div className="menu-list-container">
          {CATEGORIES.map((category, index) => {
            const { id } = category;
            const menuList = MENU.filter(menuItem => menuItem.category === id );
            if (menuList.length === 0) {
              return null;
            }
            return (
              <Category key={index} category={category} menuList={menuList} />
            )
          })}
        </div>
      </div>
    </Fragment>
  )
}

class Category extends Component {
  state = {
    showMenu: true,
  }

  toggleMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu,
    });
  }

  render() {
    const { showMenu } = this.state;
    const { category = {}, menuList = [] } = this.props;
    const { name, id } = category;
    const icon = showMenu ? 'chevron down' : 'chevron up';
    const titleClass = cx('category-title', {
      'hide-menu': !showMenu
    });

    return (
      <div className="category-container" id={id}>
        <div className="separator"></div>
        <div className={titleClass}>
          <div className="title-content">{name || 'Others'}</div>
          <div className="toggle-icon" onClick={this.toggleMenu}>
            <Icon name={icon} />
          </div>
        </div>
        {showMenu &&
          <div>
            {_.sortBy(menuList, 'thumbnail').reverse().map((menuItem, index) => {
              return (
                <Menu menu={menuItem} key={index} />
              )
            })}
          </div>
        }
      </div>
    )
  }
}
