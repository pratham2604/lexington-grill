import { TEMPLATE_TYPES } from './templates/index';

const APP_TITLE = 'Lexington Grill';

export default {
  APP_TITLE,
  DEFAULT_TEMPLATE: TEMPLATE_TYPES.GUEST,
};