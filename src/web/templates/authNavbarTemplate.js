import React, { Fragment, Component } from 'react';
import { Menu, Container, Button, Image, Icon, Responsive, Label } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import Logo from '../../assets/SmartOrdr-Logo-White.svg';
import { withRouter } from 'react-router-dom';

const getWidth = () => {
  const isSSR = typeof window === 'undefined';
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

class AuthNavbar extends Component {
  onClick = (route, title) => {
    
    const { history } = this.props;
    history.push(route);
  }

  render() {
    const { children, pageTitle, orders = [] } = this.props;
    const confirmedOrders = orders.filter(order => order.statusCode === 0).length;

    return (
      <Fragment>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
          <Menu secondary={true} size='large' className="menu-nvabar">
            <Container>
              <Menu.Item as='h3'>
                <Image src={Logo} size="mini" />
                <span className="app-title" onClick={this.onClick.bind(this, '/', 'Home')}>Smart Ordr</span>
              </Menu.Item>
              <Menu.Item position='right'>
                {orders.length > 0 &&
                  <span className="account" onClick={this.onClick.bind(this, '/orders', 'Orders')}>
                    Orders
                    {confirmedOrders > 0 &&
                      <Label color='teal' className="orders-count">
                        {confirmedOrders}
                      </Label>
                    }
                  </span>
                }
                <Button primary animated='vertical' as="a" onClick={this.onClick.bind(this, '/checkout', 'Checkout')}>
                  <Button.Content visible>Checkout</Button.Content>
                  <Button.Content hidden>
                    <Icon name='shop' />
                  </Button.Content>
                </Button>
              </Menu.Item>
            </Container>
          </Menu>
        </Responsive>
        <Container fluid className="module-container">
          {children}
        </Container>
      </Fragment>
    );
  }
}

export default (withRouter(AuthNavbar));