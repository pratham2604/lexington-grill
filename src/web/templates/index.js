import GuestTemplate from './guestTemplate';
import AuthNavbarTemplate from './authNavbarTemplate';

export const TEMPLATE = {
  GUEST: GuestTemplate,
  AUTH_NAVBAR: AuthNavbarTemplate,
};

export const TEMPLATE_TYPES = {
  GUEST: 'GUEST',
  AUTH_NAVBAR: 'AUTH_NAVBAR',
};