import React, { Fragment, Component } from 'react';
import { Menu, Container, Image } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import Logo from '../../assets/SmartOrdr-Logo-White.svg';
import { withRouter } from 'react-router-dom';

export default class extends Component {
  render() {
    const { children, pageTitle, orders } = this.props;
    return (
      <Fragment>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <GuestMenu fixed orders={orders}/>
        <GuestMenu orders={orders} />
        <Container fluid className="module-container">
          {children}
        </Container>
      </Fragment>
    );
  }
}

class GuestMenuWithRouter extends Component {
  onClick = (route) => {
    const { history } = this.props;
    history.push(route);
  }

  render() {
    const { fixed } = this.props;
    const style = fixed ? {} : {margin: 0};
    const fixedStyle = fixed ? 'top': null;

    return (
      <Menu secondary={true} size='large' className="guest-menu-nvabar" fixed={fixedStyle} style={style}>
        <Container>
          <Menu.Item as='h3'>
            <Image src={Logo} size="mini" />
            <span className="app-title" onClick={this.onClick.bind(this, '/')}>Lexington Grill</span>
          </Menu.Item>
        </Container>
      </Menu>
    )
  }
}

const GuestMenu = withRouter(GuestMenuWithRouter);