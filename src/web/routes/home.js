import Config from '../config';
import { TEMPLATE_TYPES } from '../templates/index';
import HomeComponent from '../../modules/home/UI/index';

export default {
  Component: HomeComponent,
  path: '/',
  title: Config.APP_TITLE,
  templateType: TEMPLATE_TYPES.GUEST,
};